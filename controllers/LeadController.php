<?php
use Illuminate\Database\Capsule\Manager as DB;

class LeadController extends Controller
{
    public function createLead($request, $response, $args)
    {
        $data = $request->getParams();
        $data['created_at'] = date('Y-m-d G:i:s', time());
        $data['updated_at'] = $data['created_at'];
        $lead = Lead::create($data);
        Log::write('Добавлен новый лид "'.$lead->name.'"', 'lead', $lead->id);
        return $response->withStatus(302)->withHeader('Location', '/admin/leads');
    }

    public function editLead($request, $response, $args)
    {
        $data = $request->getParams();
        $tags = $data['tags'];
        $data['tags'] = implode(',', $tags);
        $id = $request->getAttribute('id');

        $lead = Lead::find($id);
        foreach ($data as $key => $value) {
            $lead->$key = $value;
        }

        if ($lead->client == 'on') {
            $lead->client = 1;
        }

        $lead->updated_at = date('Y-m-d G:i:s', time());
        $lead->save();
        Log::write('Изменен лид "'.$lead->name.'"', 'lead', $lead->id);

        DB::table('tags')->where('entity_id', '=', $lead->id)->delete();
        foreach ($tags as $tag) {
            DB::table('tags')->insert(['name' => $tag, 'entity_id' => $lead->id]);
        }
        return $response->withStatus(302)->withHeader('Location', '/admin/leads');
    }

    public function deleteLead($request, $response, $args)
    {
        $id = $args['id'];
        $lead = Lead::find($id);
        Lead::destroy($id);
        Log::write('Удален лид "'.$lead->name.'"', 'lead', $id);
        return $response->withStatus(302)->withHeader('Location', '/admin/leads');
    }



}