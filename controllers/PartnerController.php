<?php
use Illuminate\Database\Capsule\Manager as DB;

class PartnerController extends Controller
{
    public function createPartner($request, $response, $args)
    {
        $data = $request->getParams();
        $partner = Partner::create($data);
        Log::write('Добавлен новый водитель "'.$partner->name.'"', 'partner', $partner->id);
        return $response->withStatus(302)->withHeader('Location', '/admin/partners');
    }

    public function editPartner($request, $response, $args)
    {
        $data = $request->getParams();
        Partner::find($args['id'])->update($data);
        Log::write('Изменен водитель "'.$data['name'].'"', 'partner', $args['id']);
        return $response->withStatus(302)->withHeader('Location', '/admin/partners');
    }

    public function deletePartner($request, $response, $args)
    {
        $partner = Partner::find($args['id']);
        $partner->delete();
        $partner->transports()->delete();
        return $response->withStatus(302)->withHeader('Location', '/admin/partners');
    }



}