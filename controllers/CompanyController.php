<?php
use Illuminate\Database\Capsule\Manager as DB;

class CompanyController extends Controller
{
    public function createCompany($request, $response, $args)
    {
        $data = $request->getParams();
        $company = Company::create($data);
        if ($_FILES['pics']['size'][0] > 0) {
            $files = $this->_uploadFiles('pics');
            $files_id = [];
            foreach ($files as $file) {
                $files_id[] = $file['id'];
            }
            $company->files()->sync($files_id);
        }
        return $response->withStatus(302)->withHeader('Location', '/admin/company');
    }

    public function editCompany($request, $response, $args)
    {
        $data = $request->getParams();
        $company = Company::find($args['id']);
        if ($_FILES['pics']['size'][0] > 0) {
            $files = $this->_uploadFiles('pics');
            $files_id = [];
            foreach ($files as $file) {
                $files_id[] = $file['id'];
            }
            $new_files = Company::with('files')->where('id',$args['id'])->get()->toArray();
            foreach ($new_files[0]['files'] as $value) {
                $files_id[] = $value['id'];
            }
            $company->files()->sync($files_id);
        } else {
            $company->update($data);
        }
        return $response->withStatus(302)->withHeader('Location', '/admin/company');
    }

    public function editCompanyRequisites($request, $response, $args)
    {
        $data = $request->getParams();
        $data['company_id'] = $args['id'];
        $requisites = DB::table('requisites')->where('company_id', '=', $args['id'])->get()->first();
        if ($requisites == FALSE) {
            DB::table('requisites')->insert($data);
        } else {
            DB::table('requisites')->where('company_id', '=', $args['id'])->update($data);
        }
        return $response->withStatus(302)->withHeader('Location', '/admin/company');
    }


    public function deleteLead($request, $response, $args)
    {
        $id = $args['id'];
        $lead = Lead::find($id);
        Lead::destroy($id);
        Log::write('Удален лид "'.$lead->name.'"', 'lead', $id);
        return $response->withStatus(302)->withHeader('Location', '/admin/leads');
    }

    public function autocompleteCompany($request, $response, $args) {
        $params = $request->getQueryParams();

        $company = Company::where('name', 'LIKE', '%'.$params['query'].'%')->get();

        if ($company) {
            $company = $company->toArray();
            $massiv['query'] = 'Unit';
            foreach ($company as $comp) {
                $massiv['suggestions'][] = ['value' => $comp['name'], 'data' => $comp ] ;
            }
            $json = json_encode($massiv);
            return $json;
        } else {
            $massiv['suggestions'] = '';
            return json_encode($massiv);
        }
    }


}