<?php

class UserController extends Controller
{
    public function showLoginPage($request, $response, $args)
    {
        $user = $this->ci['sentinel']->check();
        if($user && $this->ci['sentinel']->inRole('admin')) {
            return $response->withStatus(301)->withHeader('Location', '/admin');
        } else {
            $messages = $this->ci['flash']->getMessages();
            return $this->ci->view->render($response, 'login.html.twig' , ['messages' => $messages]);
        }

    }

    public function doLogin($request, $response, $args)
    {
        $data = $request->getParams();
        $credentials = [
            'email'    => $data['login'],
            'password' => $data['pass'],
        ];

        if (isset($data['remember']) && $data['remember']) {
            $user = $this->ci['sentinel']->authenticateAndRemember($credentials);
        } else {
            $user = $this->ci['sentinel']->authenticate($credentials);
        }

        if ($user) {
            return $response->withStatus(301)->withHeader('Location', '/');
        } else {
            $this->ci['flash']->addMessage('wrong', 'Неверные данные');
            return $response->withStatus(301)->withHeader('Location', '/login');
        }
    }

    public function doLogout($request, $response, $args)
    {
        $this->ci['sentinel']->logout();
        return $response->withStatus(301)->withHeader('Location', '/login');
    }

    public function createUser($request, $response, $args)
    {
        $data = $request->getParams();
        $r = $data['role'];
        unset($data['role']);

        $user = $this->ci['sentinel']->registerAndActivate($data);
        $role = $this->ci['sentinel']->findRoleBySlug($r);
        $role->users()->attach($user);
        return $response->withStatus(301)->withHeader('Location', '/admin/users');
    }

    public function editUser($request, $response, $args)
    {
        $data = $request->getParams();
        //Если пароль пустой, то не нужно обновлять информацию о нем
        if (strlen($data['password']) == 0) {
            unset($data['password']);
        }
        $user = User::with('role')->find($args['id']);
        $this->ci['sentinel']->update($user, $data);
        
        $prevRole = $user['role'][0]['id'];
        $prevRole = $this->ci['sentinel']->findRoleById($prevRole);
        $prevRole->users()->detach($user);

        $r = $data['role'];
        $role = $this->ci['sentinel']->findRoleBySlug($r);
        $role->users()->attach($user);

        return $response->withStatus(301)->withHeader('Location', '/admin/users');
    }

    public function getRole($request, $response, $args) {
        $id = $request->getParams()['id'];
        echo Role::find($id);
    }

    public function deleteUser($request, $response, $args) {
        if ($args['id'] == $_SESSION['user']['id']) {
            throw new \Exception('Вы не можете удалить свою учетную запись<br><a href="/admin/users">Вернуться назад</a>');
        }

        $user = User::find($args['id']);

        if ($user) {
            $user->role()->detach();
            $user->delete();
        }
        return $response->withStatus(301)->withHeader('Location', '/admin/users');
    }

    public function createRole($request, $response, $args) {
        $data = $request->getParams();

        $role = [];
        if (isset($data['id'])) {
            $role['id'] = $data['id'];
            unset($data['id']);
        }

        $role['name'] = $data['name'];
        $role['slug'] = $data['slug'];
        
        unset($data['name']);
        unset($data['slug']);

        foreach ($data as $key => $value) {
            $role['permissions'][$key] = $value == 'true' ? true : false;
        }

        $role['permissions'] = json_encode($role['permissions']);

        if ($role['id']) {
            $r = Role::find($role['id']);
            $r->update($role);
        } else {
            Role::create($role);
        }

        return $response->withStatus(301)->withHeader('Location', '/admin/users/roles');
    }

}