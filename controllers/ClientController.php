<?php
use Illuminate\Database\Capsule\Manager as DB;

class ClientController extends Controller
{
    public function createClient($request, $response, $args)
    {
        $data = $request->getParams();
        $data['phone_mobile'] = preg_replace('~[^0-9]+~','',$data['phone_mobile']);
        $data['phone_mobile2'] = preg_replace('~[^0-9]+~','',$data['phone_mobile2']);
        if (strlen($data['phone_mobile2']) < 2 ) {
            $old_client = Client::where('phone_mobile', '=', $data['phone_mobile'])
                ->orWhere('phone_mobile2', '=', $data['phone_mobile'])
                ->first();
        } else {
            $old_client = Client::where('phone_mobile', '=', $data['phone_mobile'])
                ->orWhere('phone_mobile2', '=', $data['phone_mobile'])
                ->orWhere('phone_mobile2', '=', $data['phone_mobile2'])
                ->orWhere('phone_mobile', '=', $data['phone_mobile2'])->first();
        }

        if ($old_client) {
            throw new \Exception('Клиент с таким номером уже существует');
        } else {
            $client = Client::create($data);
        }

        return $response->withStatus(302)->withHeader('Location', '/admin/clients');
    }

    public function editClient($request, $response, $args)
    {
        $data = $request->getParams();
        $data['phone_mobile'] = preg_replace('~[^0-9]+~','',$data['phone_mobile']);
        $data['phone_mobile2'] = preg_replace('~[^0-9]+~','',$data['phone_mobile2']);
        if (strlen($data['phone_mobile2']) < 2 ) {
            $old_client = Client::where('phone_mobile', '=', $data['phone_mobile'])->where('id', '!=', $args['id'] )
                ->orWhere('phone_mobile2', '=', $data['phone_mobile'])->where('id', '!=', $args['id'] )
                ->first();
        } else {
            $old_client = Client::where('phone_mobile', '=', $data['phone_mobile'])->where('id', '!=', $args['id'] )
                ->orWhere('phone_mobile2', '=', $data['phone_mobile'])->where('id', '!=', $args['id'] )
                ->orWhere('phone_mobile2', '=', $data['phone_mobile2'])->where('id', '!=', $args['id'] )
                ->orWhere('phone_mobile', '=', $data['phone_mobile2'])->where('id', '!=', $args['id'] )->first();
        }

        if ($old_client) {
            throw new \Exception('Клиент с таким номером уже существует');
        } else {
            Client::find($args['id'])->update($data);
        }
        return $response->withStatus(302)->withHeader('Location', '/admin/clients');
    }

    public function deleteClient($request, $response, $args)
    {
        $client = Client::find($args['id']);
        Client::destroy($args['id']);
        return $response->withStatus(302)->withHeader('Location', '/admin/clients');
    }

    public function exportCSV($request, $response, $args) {
        if (!is_dir('csv'))
            mkdir('csv');

        $file_name = 'csv/clients_' . date('j-m-Y') . '.csv';
        $csv = fopen($file_name, 'w');

        if (!$csv) die;

        $clients = Client::with('company')->get();
        $clients = $clients ? $clients->toArray() : [];

        if (isset($clients[0])) {
            unset($clients[0]['company_id']);
            $csv_head = array_keys($clients[0]); 

            $lit = array(
              "id" => "№",
              "name" => "ФИО Клиента",
              "phone_mobile" => "Мобильный телефон",
              "email" => "Email",
              "company" => "Компания",
              "description" => "Описание",
              "birthday" => "Дата рождения",
              "job_title" => "Должность",
              "phone_dop" => "Дополнительный телефон",
              "phone_mobile2" => "Мобильный телефон 2"
            );

            foreach ($csv_head as $key => $value) {
                $csv_head[$key] = strtr($value, $lit);
            }

            $csv_head = implode(';', $csv_head);
            fwrite($csv, mb_convert_encoding($csv_head, 'cp1251') . "\n");            
        }
        
        foreach ($clients as $client) {
            unset($client['company_id']);
            $client['company'] = isset($client['company']['name']) ? $client['company']['name'] : [];
            $str = implode(';', $client);
            fwrite($csv, mb_convert_encoding($str, 'cp1251') . "\n");
        }

        fclose($csv);
        echo "<a class=\"download-csv\" href=\"/{$file_name}\" style=\"margin-left: 25px;\"> Скачать файл</a>";
    }
}