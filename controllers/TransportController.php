<?php
use Illuminate\Database\Capsule\Manager as DB;

class TransportController extends Controller
{
    public function createTransport($request, $response, $args)
    {
        $data = $request->getParams();
        $transport = Transport::create($data);
        if ($_FILES['pics']['size'][0] > 0) {
            $files = $this->_uploadFiles('pics');
            $files_id = [];
            foreach ($files as $file) {
                $files_id[] = $file['id'];
            }
            $transport->files()->sync($files_id);
        }
        return $response->withStatus(302)->withHeader('Location', '/admin/partner/'.$transport->partner_id);
    }

    public function editTransport($request, $response, $args)
    {
        $data = $request->getParams();
        $transport = Transport::find($args['id']);
        if ($_FILES['pics']['size'][0] > 0) {
            $files = $this->_uploadFiles('pics');
            $files_id = [];
            foreach ($files as $file) {
                $files_id[] = $file['id'];
            }
            $new_files = Transport::with('files')->where('id',$args['id'])->get()->toArray();
            foreach ($new_files[0]['files'] as $value) {
                $files_id[] = $value['id'];
            }
            $transport->files()->sync($files_id);
        } else {
            $transport->update($data);
        }
        return $response->withStatus(302)->withHeader('Location', '/admin/partner/'.$data['partner_id']);
    }

    public function deleteTransport($request, $response, $args)
    {
        $transport = Transport::find($args['id']);
        Transport::destroy($args['id']);
        Log::write('Удален транспорт "'.$transport->name.'"', 'transport', $transport->id);
        return $response->withStatus(302)->withHeader('Location', '/admin/partner/'.$transport->partner_id);
    }

    public function createSettingsTransport($request, $response, $args)
    {
        $data = $request->getParams();
        DB::table('settings-transport')->insert($data);
        return $response->withStatus(302)->withHeader('Location', '/admin/settings/transport');
    }
    public function updateSettingsTransport($request, $response, $args)
    {
        $data = $request->getParams();
        DB::table('settings-transport')->where('id', $args['id'])->update($data);
        return $response->withStatus(302)->withHeader('Location', '/admin/settings/transport');
    }

    public function createSettingsTransportRepair($request, $response, $args)
    {
        $data = $request->getParams();
        TransportRepair::insert($data);
        return $response->withStatus(302)->withHeader('Location', '/admin/settings/transport_repair');
    }

    public function updateSettingsTransportRepair($request, $response, $args)
    {
        $data = $request->getParams();
        TransportRepair::where('id', $args['id'])->update($data);
        return $response->withStatus(302)->withHeader('Location', '/admin/settings/transport_repair');
    }

    public function exportCSV($request, $response, $args) {
        if (!is_dir('csv'))
            mkdir('csv');

        $file_name = 'csv/transports_' . date('j-m-Y') . '.csv';
        $csv = fopen($file_name, 'w');

        if (!$csv) die;

        $transports = Transport::with('partner', 'kinds')->get();
        $transports = $transports ? $transports->toArray() : [];
        if (isset($transports[0])) {

            $csv_head = ['№', 'Партнер', 'Вид техники', 'Цена за час', 'Цена за час (БЕЗНАЛ)', 'Доставка по Абакану', 'Доставка Межгород'];
            $csv_head = implode(';', $csv_head);
            fwrite($csv, mb_convert_encoding($csv_head, 'cp1251') . "\n");
        }

        foreach ($transports as $transport) {

            $record = [
                $transport['id'], $transport['partner']['name'], $transport['kinds']['name'],
                $transport['price_hour'], $transport['price_hour_beznal'], $transport['delivery_abakan'],
                $transport['delyvery_city']
            ];
            foreach ($record as $key => $value) {
                $value = str_replace(",", ".", $value);
                if (strlen($value) < 1) {$value == 'нет значения';}
                $record[$key] = $value;
            }
            $str = implode(';', $record);
            fwrite($csv, mb_convert_encoding($str, 'cp1251') . "\n");
        }

        fclose($csv);
        echo "<a class=\"download-csv\" href=\"/{$file_name}\" style=\"margin-left: 25px;\"> Скачать файл</a>";
    }

}