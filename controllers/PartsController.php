<?php
use Illuminate\Database\Capsule\Manager as DB;

class PartsController extends Controller
{
    public function __construct() {

    }

    public function createParts($request, $response, $args)
    {
        $data = $request->getParams();
        
        $transport = isset($data['transport']) ? $data['transport'] : null;
        if ($transport) {
            unset($data['transport']);
        }

        $part = Parts::create($data['part']);
        
        if ($transport) {
            $part->transport()->sync($transport);
        }
        Log::write('Добавлена новая Запчасть "'.$data['part']['title'].'"', 'parts', $part->id);
        echo $part->id;
        return $response->withStatus(302)->withHeader('Location', '/admin/parts');
    }

    public function editParts($request, $response, $args)
    {
        $data = $request->getParams();
        $transport = $data['transport'];
        unset($data['transport']);
        $part = Parts::find($args['id']);
        $part->update($data['part']);
        if ($transport) {
            $part->transport()->sync($transport);
        }
        Log::write('Изменена запчасть "'.$data['part']['title'].'"', 'parts', $args['id']);
        return $response->withStatus(302)->withHeader('Location', '/admin/parts');
    }

    public function deleteParts($request, $response, $args)
    {
        $part = Parts::find($args['id']);
        if ($part) {
            $p = $part->toArray();
            $part->delete();
            Log::write('Удалена запчасть "'.$p['title'].'"', 'parts', $p['id']);              
        }
        return $response->withStatus(302)->withHeader('Location', '/admin/parts');
    }

    public function deletePartsForever($request, $response, $args)
    {
        $part = Parts::withTrashed()->find($args['id']);
        if ($part) {
            $part->forceDelete();            
        }
        return $response->withStatus(302)->withHeader('Location', '/admin/parts/trashed');
    }

    public function restorePart($request, $response, $args) {
        $part = Parts::withTrashed()->find($args['id']);
        if($part) {
            $part->restore();
        }
        return $response->withStatus(302)->withHeader('Location', '/admin/parts/trashed');
    }

    public function getPartsForTech($request, $response, $args) {
        echo Parts::with('transport')->orderBy('title', 'asc')->get()->toJson();
    }

    public function getSortedParts($id = null) {
        $parts = Parts::with('transport')->orderBy('title', 'asc')->get();

/*
        $parts['for_tech'] = Parts::whereHas('transport', function($q) use($id) {
            $q->where('transportRepair_id', '=', $id);
        })->orderBy('title', 'asc')->get();
        $parts['for_tech'] = $parts['for_tech'] ? $parts['for_tech']->toArray() : [];

        $parts_not_for_tech = Parts::whereHas('transport', function($q) use($id) {
            $q->where('transportRepair_id', '<>', $id);
        })->orderBy('title', 'asc')->get();
        $parts_not_for_tech = $parts_not_for_tech ? $parts_not_for_tech->toArray() : [];

        $parts_id = array_map(
            function($el) {
                return $el['id'];
            }, 
            $parts['for_tech']
        );
        
        $parts['not_for_tech'] = array_filter(
            $parts_not_for_tech, 
            function($el) use($parts_id) {
                if (!in_array($el['id'], $parts_id)) return $el; 
            }
        );*/

        return $parts;
    }

    public function autocompleteParts($request, $response, $args) {
        $params = $request->getQueryParams();
        $parts = Parts::where('title', 'LIKE', '%'.$params['query'].'%')->orderBy('title', 'asc')->get()->toArray();

        if ($parts) {
            $massiv['query'] = 'Unit';
            foreach ($parts as $part) {
                $massiv['suggestions'][] = ['value' => $part['title'], 'data' => $part ] ;
            }
            $json = json_encode($massiv);
            return $json;
        } else {
            $massiv['suggestions'] = '';
            return json_encode($massiv);
        }
    }

    public function exportCSV($request, $response, $args) {
        $parts = Parts::orderBy('title', 'asc')->get()->toArray();

        if (!is_dir('csv'))
            mkdir('csv');

        $file_name = 'csv/parts.csv';
        $csv = fopen($file_name, 'w');

        if (!$csv) die;

        $table_head = ["№", "Запчасть/Расходник", "Количество", "Ед.изм", "Цена", "Примечание"];
       
        $table_head = implode(';', $table_head);
        fwrite($csv, mb_convert_encoding($table_head, 'cp1251') . "\n"); 

        foreach ($parts as $key => $value) {
            $f = [$key + 1, $value['title'], $value['count'], $value['unit'], $value['price'], $value['note']];
            $f = implode(';', $f);
            fwrite($csv, mb_convert_encoding($f, 'cp1251') . "\n"); 
        }

        fclose($csv);
        
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=Запчасти.csv');
        header('Pragma: no-cache');
        readfile($file_name);
    }

}