<?php
use Illuminate\Database\Capsule\Manager as DB;

class RepairController extends Controller
{
    private function setPartsMovement($data, $repair) {
        foreach ($data['table'] as $key => $value) {
    		if ($value['count'] <= 0)
    			return 'В строке №' . ($key+1) . ' указанно некорректное количество запчастей (Кол-во должно быть больше 0)';
    	}

        foreach ($data['table'] as $row) {
            if (!$row['repair_job'] && !$row['parts_id']) continue;
            // if ($row['count'] <= 0) continue;

            $part = Parts::find($row['parts_id']);
            if (!$part) continue;

            $movement_data = [
                'entity_type' => 'Repair',
                'parts_id' => $part->id,
                'price' => $part['price'],
                'entity_id' => $repair->id,
                'description' => $row['repair_job'],
                'count' => $row['count'],
                'date' => date('Y-m-d', strtotime($data['form']['date'])),
                'transport_id' => $data['form']['transportRepair_id']
            ];

            if (isset($row['movement_id']) && $row['movement_id']) {
                $update_movement = Movement::find($row['movement_id']);

                if ($update_movement->parts_id != $row['parts_id']) {
                    $old_part = Parts::find($update_movement->parts_id);
                    $old_part->count += $update_movement->count;
                    $old_part->save();

                    // $part->count = $part->count <= $row['count'] ? 0 : $part->count - $row['count'];
                    $part->count -= $row['count'];
                } else {
                    $diff = $row['count'] - $update_movement->count;
                    $part->count -= $diff;     
                }

                $part->save(); 
                $update_movement->update($movement_data);

            } else {
                // $part->count = $part->count <= $row['count'] ? 0 : $part->count - $row['count'];
                $part->count -= $row['count'];
                $part->save();
                Movement::create($movement_data);               
            }
        }        
    }

    public function createRepair($request, $response, $args)
    {
        $data = $request->getParams();
        array_walk_recursive($data, function($key, &$value) {
            if (!$value) $value = null;
        });

        $form = $data['form'];
        $form['date'] = $form['date'] ? date("Y.m.d", strtotime($form['date'])) : date("Y.m.d");

        $repair = Repair::create($form);
        Log::write('Добавлен отчет о ремонте №'.$repair->id, 'repair', $repair->id);            
        
        $this->setPartsMovement($data, $repair);
    }

    public function updateRepair($request, $response, $args) {
        $data = $request->getParams();
        array_walk_recursive($data, function($key, &$value) {
            if (!$value) $value = null;
        });

        $form = $data['form'];
        $form['date'] = $form['date'] ? date("Y.m.d", strtotime($form['date'])) : date("Y.m.d");
        
        $movement_id = isset($data['movement_id']) ? $data['movement_id'] : [];

        $repair = Repair::find($form['id']);
        $repair->update($form);
        Log::write('Обновлен отчет о ремонте №'.$repair->id, 'repair', $repair->id);

        $repair->load('movement');
        $movement = $repair->movement;

        if ($movement) {
            $movement = $movement->toArray();
            $arr = array_map(function($elem) { return $elem['id']; }, $movement);
            $diff = array_diff($arr, $movement_id);
            foreach ($diff as $id) {
                $m = Movement::find($id);
                $p = Parts::find($m->parts_id);
                $p->count = $p->count + $m->count;
                $p->save();
                Movement::destroy($id);
            }
        }

        $this->setPartsMovement($data, $repair);
    }

    public function deleteRepair($request, $response, $args) {
        $id = $request->getParams()['id'];
        $repair = Repair::with('movement')->find($id);
        foreach ($repair['movement'] as $key => $value) {
        	$part = Parts::find($value['part']['id']);
        	$part->count += $value['count'];
        	$part->save();
        	Movement::destroy($value['id']);
        }
        $repair->delete();
    }

}