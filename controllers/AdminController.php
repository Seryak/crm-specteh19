<?php
use Illuminate\Database\Capsule\Manager as DB;

class AdminController extends Controller
{
    
    public function showRepairs($request, $response, $args)
    {
        $repairs = Repair::with('transport')->with('drivers')->orderBy('date', 'desc')->get();
        return $this->ci->view->render($response, 'admin/repair/repair.html.twig', ['repairs' => $repairs]);
    }


    public function showRepairAdd($request, $response, $args)
    {
        $vars = ['transport' => TransportRepair::all()->sortBy('name')];
        $vars['drivers'] = Driver::orderBy('name')->get();
        
        if (isset($args['id']) && $args['id']) {
            $vars['repair'] = Repair::with('movement')->find($args['id']);

            if ($vars['repair']['transportRepair_id']) {
                $id = $vars['repair']['transportRepair_id'];
                $vars['parts'] = Parts::with('transport')->orderBy('title', 'asc')->get();
            }
        }     
        return $this->ci->view->render($response, 'admin/repair/repair-add.html.twig', $vars);
    }



    public function showSubreports($request, $response, $args)
    {
        $reports = Subreport::with('employer');

        if (isset($_GET['sortby']) && in_array($_GET['sortby'], ['date_in', 'date_out'])) {
            $sort = isset($_GET['sort']) && $_GET['sort'] == 'asc' ? 'asc' : 'desc';
            $reports = $reports->orderBy($_GET['sortby'], $sort)->get();

        } else {
            $reports = $reports->orderBy('date_out', 'desc')->get();
        }

        return $this->ci->view->render($response, 'admin/subreport/subreports.html.twig', ['reports' => $reports]);
    }

    public function showSubreportAdd($request, $response, $args)
    {
        $vars = ['drivers' => Driver::orderBy('name')->get()->toJson()];
        $vars['parts'] = Parts::get()->toJson();

        if (isset($args['id'])) {
        	$subreport = Subreport::with(['movement' => function($q)
            {
                $q->with('part');
            }])->find($args['id']);
        	if ($subreport) $vars['subreport'] = $subreport->toArray();
        }
            
        return $this->ci->view->render($response, 'admin/subreport/subreport-add.html.twig', $vars);
    }

    public function showMessage($request, $response, $args)
    {
        switch ($args['message']) {
            case 'no-access':
                $message = 'У вас нет доступа для этой операции';
                break;
        }
        return $this->ci->view->render($response, 'admin/message.html.twig', ['message' => $message]);
    }

    public function showLeadList($request, $response, $args)
    {
        $leads = Lead::where([
            ['created_at', '>' , '2016-10-07 10:18:21'],
            ['tags', 'NOT LIKE', '%архив%'],
            ['client', '=', 0],
        ])->get()->sortByDesc("created_at");
        return $this->ci->view->render($response, 'admin/leads.html.twig', ['leads' => $leads]);
    }

    public function showLeadAdd($request, $response, $args)
    {
        return $this->ci->view->render($response, 'admin/leads-add.html.twig');
    }

    public function showLeadEdit($request, $response, $args)
    {
        $id = $request->getAttribute('id');
        $records = DB::table('tags')->where('entity_id', '=', $id)->get();
        $tags = [];
        foreach ($records as $record) {
            $tags[] = $record->name;
        }
        $lead = Lead::find($id);
        return $this->ci->view->render($response, 'admin/leads-add.html.twig', ['lead' => $lead, 'tags' => $tags]);
    }

    public function showCompanyAdd($request, $response, $args)
    {
        return $this->ci->view->render($response, 'admin/company-add.html.twig');
    }

    public function showCompanyList($request, $response, $args)
    {
        $companies = Company::with('client','responsibles')->orderBy('name', 'asc')->get()->toArray(); 
        return $this->ci->view->render($response, 'admin/company.html.twig', ['company' => $companies]); 
    }

    public function showCompanyEdit($request, $response, $args)
    {
        return $this->ci->view->render($response, 'admin/company-add.html.twig', ['company' => Company::with('client','responsibles')->find($args['id'])]);
    }

    public function showCompanyRequisites($request, $response, $args)
    {
        $requisites = DB::table('requisites')->where('company_id', '=', $args['id'])->first();
        $company = Company::find($args['id']);
        return $this->ci->view->render($response, 'admin/requisites.html.twig', ['requisites' => $requisites, 'id' => $args['id'], 'company' => $company]);
    }

    public function showCompanyWorkers($request, $response, $args)
    {
        $clients = Client::where('company_id', '=', $args['id'])->get()->sortBy('name')->toArray();
        $company = Company::find($args['id']);
        $companies = Company::all()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/clients.html.twig', ['clients' => $clients, 'id' => $args['id'], 'company' => $company, 'companies' => $companies]);
    }


    public function showClientAdd($request, $response, $args)
    {
        $companies = Company::all()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/clients-add.html.twig', ['companies' => $companies]);
    }

    public function showClientList($request, $response, $args)
    {
        $data = $request->getParams();
        $ar2 = [];
        $clients = Client::with('company')->get()->sortBy('name')->toArray();
        if (isset($data['sortby'])) {
            $sort = $data['sort'] == 'asc' ? SORT_ASC : SORT_DESC;
            foreach ($clients as $client) {
                $ar2[] = $client[$data['sortby']]['name'];
            }
        } else {
            $sort = SORT_ASC;
            foreach ($clients as $client) {
                $ar2[] = $client['name'];
            }
        }
        array_multisort($ar2, $sort, $clients, $sort);

        $companies = Company::all()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/clients.html.twig', ['clients' => $clients, 'companies' => $companies]);
    }

    public function showClientEdit($request, $response, $args)
    {
        $client = Client::find($args['id']);
        $companies = Company::all()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/clients-add.html.twig', ['client' => $client, 'companies' => $companies]);
    }

    public function showDriverAdd($request, $response, $args)
    {
        return $this->ci->view->render($response, 'admin/driver/drivers-add.html.twig');
    }

    public function showDriverList($request, $response, $args)
    {
        $drivers = Driver::all()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/driver/drivers.html.twig', ['drivers' => $drivers]);
    }

    public function showDriverListTrash($request, $response, $args)
    {
        $drivers = Driver::onlyTrashed()->get()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/driver/drivers.html.twig', ['drivers' => $drivers]);
    }

    public function showDriverEdit($request, $response, $args)
    {
        $driver = Driver::withTrashed()->find($args['id']);
        return $this->ci->view->render($response, 'admin/driver/drivers-add.html.twig',  ['driver' => $driver]);
    }

    public function showPartnerAdd($request, $response, $args)
    {
        $companies = Company::all()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/partner/partners-add.html.twig', ['companies' => $companies]);
    }

    public function showPartnerList($request, $response, $args)
    {
        $partners = Partner::all()->sortBy('name')->toArray();
        $companies = Company::all()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/partner/partners.html.twig', ['partners' => $partners, 'companies' => $companies]);
    }

    public function showPartnerListTrash($request, $response, $args)
    {
        $partners = Partner::onlyTrashed()->get()->sortBy('name')->toArray();
        $companies = Company::all()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/partner/partners.html.twig', ['partners' => $partners, 'companies' => $companies]);
    }

    public function showPartnerEdit($request, $response, $args)
    {
        $partner = Partner::withTrashed()->find($args['id']);
        $companies = Company::all()->sortBy('name')->toArray();
        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/partner/partners-add.html.twig',  ['partner' => $partner, 'companies' => $companies, 'kinds_transport' => $kinds_transport]);
    }

    public function showAgentAdd($request, $response, $args)
    {
        $companies = Company::all()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/agent/agent-add.html.twig', ['companies' => $companies]);
    }

    public function showAgentList($request, $response, $args)
    {
        $agents = Agent::all()->sortBy('name')->toArray();
        $companies = Company::all()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/agent/agents.html.twig', ['agents' => $agents, 'companies' => $companies]);
    }

    public function showAgentEdit($request, $response, $args)
    {
        $agent = Agent::find($args['id']);
        $companies = Company::all()->sortBy('name')->toArray();
        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/agent/agent-add.html.twig',  ['agent' => $agent, 'companies' => $companies, 'kinds_transport' => $kinds_transport]);
    }

    public function showTransportAdd($request, $response, $args)
    {
        $partner = Partner::find($args['id']);
        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/transport/transports-add.html.twig', ['partner' => $partner, 'kinds_transport' => $kinds_transport]);
    }

    public function showTransportList($request, $response, $args)
    {
        $data = $request->getParams();
        $ar2 = [];
        
        $transports = Transport::with('partner', 'kinds')->get()->toArray();
        if (isset($data['sortby'])) {
            $sort = $data['sort'] == 'asc' ? SORT_ASC : SORT_DESC;
            foreach ($transports as $transport) {
                $ar2[] = $transport[$data['sortby']]['name'];
            }
        } else {
            $sort = SORT_ASC;
            foreach ($transports as $transport) {
                $ar2[] = $transport['kinds']['name'];
            }
        }

        array_multisort($ar2, $sort, $transports, $sort);

        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/transport/transports.html.twig', ['transports' => $transports, 'kinds_transport' => $kinds_transport]);
    }

    public function showTransportEdit($request, $response, $args)
    {
        $transport = Transport::find($args['id']);
        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/transport/transports-add.html.twig',  ['transport' => $transport, 'kinds_transport' => $kinds_transport]);
    }

    public function showParts($request, $response, $args) 
    {
        $data = $request->getParams();   
        $vars = ['transport' => TransportRepair::orderBy('name')->get()];

        /* Order by count */
        if (isset($data['sortby']) && $data['sortby'] == 'count') {
            $sort = isset($data['sort']) && $data['sort'] == 'asc' ? 'asc' : 'desc';
            $parts = Parts::orderBy('count', $sort)->get();
            return $this->ci->view->render($response, 'admin/parts/parts.html.twig', ['parts' => $parts]);
        }

        $vars['data'] = $data;
        $parts = Parts::orderBy('title', 'asc');

        if ($data) {
            if (isset($data['transport']) && $data['transport']) {
                $id = $data['transport'];
                $parts = $parts->whereHas('transport', function($q) use($id) {
                    $q->where('transportRepair_id', '=', $id);
                });
            }

            if (isset($data['title']) && $data['title'] != '') {
                $parts = $parts->where('title', 'LIKE', '%'.$data['title'].'%');
            }

            $vars['parts'] = $parts->orderBy('title', 'asc')->get();
        } else {
            $vars['parts'] = Parts::orderBy('title', 'asc')->get();
        }

        return $this->ci->view->render($response, 'admin/parts/parts.html.twig', $vars);
    }

    public function showTrashedParts($request, $response, $args) {
        $vars = ['parts' => Parts::onlyTrashed()->get()->toArray()];
        return $this->ci->view->render($response, 'admin/parts/parts-trashed.html.twig', $vars);
    }

    public function showPartTrashed($request, $response, $args) {
        $vars = ['part' => Parts::withTrashed()->find($args['id'])->toArray()];
        return $this->ci->view->render($response, 'admin/parts/parts-trashed-info.html.twig', $vars);
    }

    public function showPartsAdd($request, $response, $args) {
        $transport = TransportRepair::get()->sortBy('name');
        $transport = $transport ? $transport->toArray() : [];

        if (isset($args['id'])) {
            $part = Parts::find($args['id']);
            $part->transport;
            //dump($part->toArray());
            $part = $part ? $part->toArray() : [];
            //die;
            return $this->ci->view->render($response, 'admin/parts/parts-add.html.twig', ['part' => $part, 'transport' => $transport]);
        } else {
            return $this->ci->view->render($response, 'admin/parts/parts-add.html.twig', ['transport' => $transport]);
        }
        
    }

    public function showOrderAdd($request, $response, $args)
    {
        //Парметры фильтра, чтобы вернуть на страницу заявок с фильтрами
        $ref = $_SERVER['HTTP_REFERER'];
        $back_url = explode('?', $ref)[1];

        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();

        return $this->ci->view->render($response, 'admin/order/orders-add.html.twig', [
            'kinds_transport' => $kinds_transport,
            'drivers' => Driver::All()->sortBy('name')->toArray(),
            'partners' => Partner::all()->sortBy('name')->toArray(),
            'agents' => Agent::all()->sortBy('name')->toArray(),
            'companies' => Company::all()->sortBy('name')->toArray(),
            'back_url' => $back_url
        ]);
    }

    public function showOrderEdit($request, $response, $args)
    {
        //Парметры фильтра, чтобы вернуть на страницу заявок с фильтрами
        $ref = $_SERVER['HTTP_REFERER'];
        if (count(explode('?', $ref)) > 1 ) {
            $back_url = explode('?', $ref)[1];
        } else {
            $back_url = '';
        }

        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        $order = Order::find($args['id']);

        return $this->ci->view->render($response, 'admin/order/orders-add.html.twig', [
            'kinds_transport' => $kinds_transport,
            'drivers' => Driver::All()->sortBy('name')->toArray(),
            'partners' => Partner::withTrashed()->get()->sortBy('name')->toArray(),
            'agents' => Agent::withTrashed()->get()->sortBy('name')->toArray(),
            'companies' => Company::all()->sortBy('name')->toArray(),
            'order' => $order,
            'client' => Client::find($order->client_id),
            'back_url' => $back_url
        ]);
    }

    public function showOrderCopy($request, $response, $args)
    {
        //Парметры фильтра, чтобы вернуть на страницу заявок с фильтрами
        $ref = $_SERVER['HTTP_REFERER'];
        $back_url = explode('?', $ref)[1];

        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        $old_order = Order::find($args['id']);
        $order = $old_order->replicate();

        return $this->ci->view->render($response, 'admin/order/orders-add.html.twig', [
            'kinds_transport' => $kinds_transport,
            'drivers' => Driver::All()->sortBy('name')->toArray(),
            'partners' => Partner::all()->sortBy('name')->toArray(),
            'companies' => Company::all()->sortBy('name')->toArray(),
            'order' => $order,
            'agents' => Agent::all()->sortBy('name')->toArray(),
            'client' => Client::find($order->client_id),
            'back_url' => $back_url
        ]);
    }

    public function showOrderList($request, $response, $args)
    {
        $data = $request->getParams();

//        ddd($data);
        if (isset($data['filter'])) {
            $filter = $data['filter'];
            $orders = Order::with('kind_transport', 'client.company', 'company', 'agent');
            if ($filter['status'] != '') {
                $orders = $orders->where('status', '=', $filter['status']);
            }
            if ($filter['kind_transport'] != '') {
                $orders = $orders->where('kind_transport', '=', $filter['kind_transport']);
            }
            if ($filter['date_start']) {
                $orders = $orders->where('date_order', '>=', date("Y-m-d", strtotime($filter['date_start'])));
            }

            if ($filter['date_end']) {
                $orders = $orders->where('date_order', '<=', date("Y-m-d H:i:s",  strtotime($filter['date_end']) + (23 * 3600) + 3590));
            }

            $orders = $orders->get()->sortByDesc('date_order')->toArray();
        } else {
            $orders = Order::with('kind_transport', 'client.company', 'company', 'agent')->get()->sortByDesc('date_order')->toArray();
        }
        $new_items = [];
        $old_items = [];
        foreach ($orders as &$item) {
            if ( $item['date_order'] == '0000-00-00 00:00:00' ) {
                $new_items[] = $item;
            } else {
                $old_items[] = $item;
            }

        }
        $orders = array_merge ($new_items, $old_items);
        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        $partners = $this->allTree(Partner::all()->toArray());
        $drivers = $this->allTree(Driver::all()->toArray());
        $users = User::All()->toArray();
        $twig_vars = [
            'orders' => $orders,
            'users' => $users,
            'kinds_transport' => $kinds_transport,
            'partners' => $partners, '
            drivers' => $drivers
        ];
        if (isset($data['filter'])) {
            $twig_vars['filter'] = $filter;
            }
        return $this->ci->view->render($response, 'admin/order/orders.html.twig', $twig_vars );
    }

    public function showOrdersByClient($request, $response, $args)
    {
        $orders = Order::with('kind_transport', 'client.company', 'company', 'agent')->where('client_id', '=', $args['id'])->get()->sortByDesc('date_order')->toArray();
        $users = User::All()->toArray();
        $client = Client::find($args['id']);
        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        $partners = $this->allTree(Partner::all()->toArray());
        $drivers = $this->allTree(Driver::all()->toArray());
        return $this->ci->view->render($response, 'admin/order/orders.html.twig',  ['orders' => $orders, 'client' => $client, 'users' => $users, 'kinds_transport' => $kinds_transport, 'partners' => $partners, 'drivers' => $drivers]);
    }

    public function showOrdersByCompany($request, $response, $args)
    {
        $orders = Order::with('kind_transport', 'client.company', 'company', 'agent')->where('company_id', '=', $args['id'])->get()->sortByDesc('date_order')->toArray();
        $users = User::All()->toArray();
        $company = Company::find($args['id']);
        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        $partners = $this->allTree(Partner::all()->toArray());
        $drivers = $this->allTree(Driver::all()->toArray());
        return $this->ci->view->render($response, 'admin/order/orders.html.twig',  ['company' => $company ,'orders' => $orders, 'client' => $client, 'users' => $users, 'kinds_transport' => $kinds_transport, 'partners' => $partners, 'drivers' => $drivers]);
    }

    public function showOrdersByPartner($request, $response, $args)
    {
        $orders = Order::with('kind_transport', 'client.company', 'company', 'agent')->where('partner_id1', '=', $args['id'])->orWhere('partner_id2', '=', $args['id'])->orWhere('partner_id3', '=', $args['id'])->get()->sortByDesc('date_order')->toArray();
        $partner = Partner::find($args['id']);
        $users = User::All()->toArray();
        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        $partners = $this->allTree(Partner::all()->toArray());
        $drivers = $this->allTree(Driver::all()->toArray());
        return $this->ci->view->render($response, 'admin/order/orders.html.twig',  ['partner' => $partner, 'orders' => $orders, 'users' => $users, 'kinds_transport' => $kinds_transport, 'partners' => $partners, 'drivers' => $drivers]);
    }

    public function showOrdersByAgent($request, $response, $args)
    {
        $orders = Order::with('kind_transport', 'client.company', 'company', 'agent')->where('agent_id', '=', $args['id'])->get()->sortByDesc('date_order')->toArray();
        $agent = Agent::find($args['id']);
        $users = User::All()->toArray();
        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        $partners = $this->allTree(Partner::all()->toArray());
        $drivers = $this->allTree(Driver::all()->toArray());
        return $this->ci->view->render($response, 'admin/order/orders.html.twig',  ['agent' => $agent, 'orders' => $orders, 'users' => $users, 'kinds_transport' => $kinds_transport, 'partners' => $partners, 'drivers' => $drivers]);
    }

    public function showOrdersByDriver($request, $response, $args)
    {
        $orders = Order::with('kind_transport', 'client.company', 'company', 'agent')->where('driver_id1', '=', $args['id'])->orWhere('driver_id2', '=', $args['id'])->get()->sortByDesc('date_order')->toArray();
        $driver = Driver::find($args['id']);
        $users = User::All()->toArray();
        $kinds_transport = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        $partners = $this->allTree(Partner::all()->toArray());
        $drivers = $this->allTree(Driver::all()->toArray());
        return $this->ci->view->render($response, 'admin/order/orders.html.twig',  ['driver' => $driver, 'orders' => $orders, 'users' => $users, 'kinds_transport' => $kinds_transport, 'partners' => $partners, 'drivers' => $drivers]);
    }


    public function showUserList($request, $response, $args)
    {
        $users = User::with('role')->get()->toArray();
        return $this->ci->view->render($response, 'admin/users.html.twig', ['users' => $users]);
    }

    public function showUserRoles($request, $response, $args)
    {
        $roles = Role::all();
        return $this->ci->view->render($response, 'admin/user-roles.html.twig', ['roles' => $roles]);
    }    

    public function showUserEdit($request, $response, $args)
    {
        $roles = Role::orderBy('id', 'desc')->get();
        $user = User::with('role')->find($args['id']);
        return $this->ci->view->render($response, 'admin/users-add.html.twig', ['roles' => $roles, 'user' => $user]);
    }

    public function showUserAdd($request, $response, $args)
    {
        $roles = Role::orderBy('id', 'desc')->get();
        return $this->ci->view->render($response, 'admin/users-add.html.twig', ['roles' => $roles]);
    }



    public function showSettings($request, $response, $args)
    {
        return $this->ci->view->render($response, 'admin/settings/settings.html.twig');
    }

    public function showSettingsTransport($request, $response, $args)
    {
        $transports = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/settings/settings-transports.html.twig', ['transports' => $transports, 'model' => 'transport']);
    }

    public function showEditSettingsTransport($request, $response, $args)
    {
        $transports = DB::table('settings-transport')->get()->sortBy('name')->toArray();
        $transport = DB::table('settings-transport')->find($args['id']);
        return $this->ci->view->render($response, 'admin/settings/settings-transports.html.twig', ['transports' => $transports, 'transport' => $transport, 'model' => 'transport']);
    }

    public function showSettingsTransportRepair($request, $response, $args)
    {
        $transports = DB::table('transportRepair')->get()->sortBy('name')->toArray();
        return $this->ci->view->render($response, 'admin/settings/settings-transports.html.twig', ['transports' => $transports, 'model' => 'transport_repair']);
    }

    public function showEditSettingsTransportRepair($request, $response, $args)
    {
        $transports = DB::table('transportRepair')->get()->sortBy('name')->toArray();
        $transport = DB::table('transportRepair')->find($args['id']);
        return $this->ci->view->render($response, 'admin/settings/settings-transports.html.twig', ['transports' => $transports, 'transport' => $transport, 'model' => 'transport_repair']);
    } 

    public function deleteFile($request, $response, $args)
    {
        //Парметры фильтра, чтобы вернуть на страницу заявок с фильтрами
        $params = $request->getParams();
        if ( $params['back'] ) {
            $back_url = $params['back'];
        } else {
            $back_url = '/';
        }
        File::find($args['id'])->delete();

        return $response->withStatus(302)->withHeader('Location', $back_url);
    }

    public function checkUniq($request, $response, $args)
    {
        $data = DB::table($args['table'])->where($args['field'], $args['value'])->first();
        if ($data) {
            $message = '<div class="uniq-message">Такое значение уже есть у <b>'.$data->name.'</b></div>';
            return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
        }
        $message = '<div class="uniq-message">Такого значения нет</div>';
        return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
    }

    public function checkUniqPhone($request, $response, $args)
    {
        $count = strlen($args['value']);
        if ($count < 11 && $count > 1) {
            $message = '<div class="uniq-message exist">Недостаточно цифр в номере</div>';
            return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
        }
        $data = DB::table('clients')->where('phone_mobile', $args['value'])
            ->orWhere('phone_mobile2', $args['value'])
            ->orWhere('phone_dop', $args['value'])
            ->first();
        if ($data) {
            $message = '<div class="uniq-message exist">Такое значение уже есть у <a target="_blank" href="/admin/client/'.$data->id.'">'.$data->name.'</a></div>';
            return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
        }
        $message = '<div class="uniq-message">Такого значения нет</div>';
        return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
    }

    public function checkMobilePhone($request, $response, $args)
    {
        $count = strlen($args['value']);
        if ($count < 11 && $count > 1 ) {
            $message = '<div class="uniq-message exist">Недостаточно цифр в номере</div>';
            return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
        } else {
            $message = '<div class="uniq-message"></div>';
            return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
        }
    }

    public function autocompleteClient($request, $response, $args)
    {
        $params = $request->getParams();
        $query = preg_replace('~[^0-9]+~','',$params['query']);

        $clients = Client::where('phone_mobile', 'LIKE', '%'.$query.'%')->orWhere('phone_mobile2', 'LIKE', '%'.$query.'%')->get()->toArray();

        if (count($clients) > 0) {
            $massiv['query'] = 'Unit';
            foreach ($clients as $client) {
                $massiv['suggestions'][] = ['value' => $client['name'], 'data' => $client, 'query' => $query ] ;
            }
            $json = json_encode($massiv);
            return $json;
        } else {
            $massiv['suggestions'] = '';
            return json_encode($massiv);
        }
    }

    public function showIndex()
    {
        $this->slim->redirect('/admin/content');
    }

    public function showMovement($request, $response, $args) 
    {
        $movement = Movement::with('entity', 'transport', 'part')
            ->orderBy('date', 'desc')
            ->orderBy('entity_type', 'desc');        

        /* Удаление пустых ключей и page */
        $query = $request->getParams();
        foreach ($query as $key => $value) {
        	if (!$value || $key == 'page') {
        		unset($query[$key]);
        	}
        }

        $movement = $this->filterMovement($movement, $query)->paginate(100)->setPath('');
        $transport = TransportRepair::orderBy('name', 'asc')->get();

        return $this->ci->view->render($response, 
        	'admin/movement/movement.html.twig', 
        	[
        		'movement' => $movement->toArray(), 
        		'transport' => $transport,
        		'data' => $query
        	]
        );        
    }

    /**
     * Фильтрует Движение запчастей
     *
     * @param  Illuminate\Database\Eloquent\Builder $movement
     * @param array $query
     * @return Illuminate\Database\Eloquent\Builder
     */
    private function filterMovement(Illuminate\Database\Eloquent\Builder $movement, array $query = []) {
        if (isset($query['part_name'])) {
            $title = $query['part_name'];
            $reverse;

            if (isset($query['type']) && $query['type'] == 1) {
                $reverse = 'NOT ';
            }

            $movement->whereHas('part', function($q) use($title, $reverse) {
                $q->where('title', $reverse . 'LIKE', '%'.$title.'%');
            });
        }  

        if (isset($query['transport'])) {
            $movement->where('transport_id', '=', $query['transport']);
        }

        if (isset($query['date_start'])) {
            $movement->where('date', '>=', date("Y-m-d", strtotime($query['date_start'])));
        }

        if (isset($query['date_end'])) {
            $movement->where('date', '<=', date("Y-m-d", strtotime($query['date_end'])));
        }

        if (isset($query['entity_type'])) {
            $movement->where('entity_type', '=', $query['entity_type']);
        }

		return $movement;       
    }

    public function offConfigDB($request, $response, $args)
    {
        if (file_exists('config.php'))
            rename('config.php', 'configStop.php');
    }

    public function onConfigDB($request, $response, $args)
    {
        if (file_exists('configStop.php'))
            rename('configStop.php', 'config.php');
    }
}