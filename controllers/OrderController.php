<?php
use Illuminate\Database\Capsule\Manager as DB;

class OrderController extends Controller
{
    public function createOrder($request, $response, $args)
    {
        $data = $request->getParams();
        if (isset($data['order']['company_id'])) {
            $data['client']['company_id'] = $data['order']['company_id'];
        }
        if (!$data['order']['client_id']) {
            $phone_mobile = preg_replace('~[^0-9]+~','', $data['order']['client_phone']);
            $old_client = Client::where('phone_mobile', '=', $phone_mobile)
                ->orWhere('phone_mobile2', '=', $phone_mobile)->first();
            if ($old_client) {
                throw new \Exception('Клиент с таким номером уже существует <br> Нажми кнопку назад и выбери клиента из выпадающего списка');
            } else {
                $data['client']['phone_mobile'] = $data['order']['client_phone'];
                $client = Client::create($data['client']);
                $data['order']['client_id'] = $client->id;
            }

        }
        $order = Order::create($data['order']);
        if ($_FILES['pics']['size'][0] > 0) {
            $files = $this->_uploadFiles('pics');
            $files_id = [];
            foreach ($files as $file) {
                $files_id[] = $file['id'];
            }
            $order->files()->sync($files_id);
        }
        return $response->withStatus(302)->withHeader('Location', '/admin/orders?'.$data['dop_info']['back_url']);
    }

    public function updateOrder($request, $response, $args)
    {
        $data = $request->getParams();
        //не нужно изменять владельца заявки при редактировании
        unset($data['order']['owner_id']);

        if (!$data['order']['client_id']) {
            $data['client']['phone_mobile'] = $data['order']['client_phone'];
            $client = Client::create($data['client']);
            $data['order']['client_id'] = $client->id;
        } else {
            $client = Client::find($data['order']['client_id']);
            $client->company_id = $data['order']['company_id'];
            $client->save();
        };
        $order = Order::find($args['id']);
        $order->company->update(['dogovor' => $data['order']['dogovor']] );
        $order->update($data['order']);
        if ($_FILES['pics']['size'][0] > 0) {
            $files = $this->_uploadFiles('pics');
            $files_id = [];
//            ddd($files);
            foreach ($files as $file) {
                $files_id[] = $file['id'];
            }
            $new_files = Order::with('files')->where('id',$args['id'])->get()->toArray();
            foreach ($new_files[0]['files'] as $value) {
                $files_id[] = $value['id'];
            }
            $order->files()->sync($files_id);
        }
        return $response->withStatus(302)->withHeader('Location', '/admin/orders?'.$data['dop_info']['back_url']);
    }

    public function deleteOrder($request, $response, $args)
    {
        Order::destroy($args['id']);
        return $response->withStatus(302)->withHeader('Location', '/admin/orders');
    }

}