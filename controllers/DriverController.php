<?php
use Illuminate\Database\Capsule\Manager as DB;


class DriverController extends Controller
{
    public function createDriver($request, $response, $args)
    {
        $data = $request->getParams();
        $driver = Driver::create($data);
        Log::write('Добавлен новый водитель "'.$driver->name.'"', 'driver', $driver->id);
        return $response->withStatus(302)->withHeader('Location', '/admin/drivers');
    }

    public function editDriver($request, $response, $args)
    {
        $data = $request->getParams();
        $driver = Driver::find($args['id']);
        $driver->update($data);
        return $response->withStatus(302)->withHeader('Location', '/admin/drivers');
    }

    public function deleteDriver($request, $response, $args)
    {
        $driver = Driver::find($args['id']);
        $driver->delete();
        return $response->withStatus(302)->withHeader('Location', '/admin/drivers');
    }



}