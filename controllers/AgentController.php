<?php
use Illuminate\Database\Capsule\Manager as DB;

class AgentController extends Controller
{
    public function createAgent($request, $response, $args)
    {
        $data = $request->getParams();
        $agent = Agent::create($data);
        return $response->withStatus(302)->withHeader('Location', '/admin/agents');
    }

    public function editAgent($request, $response, $args)
    {
        $data = $request->getParams();
        Agent::find($args['id'])->update($data);
        return $response->withStatus(302)->withHeader('Location', '/admin/agents');
    }

    public function deleteAgent($request, $response, $args)
    {
        $agent = Agent::find($args['id']);
        $agent->delete();
        return $response->withStatus(302)->withHeader('Location', '/admin/agents');
    }



}