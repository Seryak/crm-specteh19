<?php
use Illuminate\Database\Capsule\Manager as DB;

class SubreportController extends Controller
{

    public function createReport($request, $response, $args)
    {
        $data = $request->getParams();

        $data['date_in'] = $data['date_in'] ? date("Y-m-d", strtotime($data['date_in'])) : date("Y-m-d");
        $data['date_out'] = $data['date_out'] ? date("Y-m-d", strtotime($data['date_out'])) : date("Y-m-d");

        $total = 0;
        $parts = [];

        $movement_id = [];
        $new_parts = [];

        $data['id'] = isset($data['id']) ? $data['id'] : null;     

        if (!isset($data['movement'])) $data['movement'] = [];
        foreach ($data['movement'] as $value) {
            if (!isset($value['part']['id']) || !$value['part']['id']) continue;
            if (array_key_exists($value['part']['id'], $parts)) continue;

            if (isset($value['id']))
                array_push($movement_id, $value['id']);

            $id = $value['part']['id'];
            unset($value['id']);
            unset($value['part']);
            unset($value['entity_id']);
            unset($value['parts_id']);

            $value['date'] = $data['date_in'];
            $total += $value['price'] * $value['count'];

            $parts[$id] = $value;
        }

        $data['total'] = $total;
        $data['balance'] = $data['inner_summ'] - $total;

        $movement = $data['movement'];
        unset($data['movement']);

        $data['id'] = isset($data['id']) ? $data['id'] : null;    
        
        $subreport = Subreport::updateOrCreate(['id' => $data['id']], $data);
        $subreport->load('movement');

        $m = $subreport->relationsToArray()['movement'];

        /* Удаление запчастей */
        if ($data['id']) {
            $in_mysql = array_map(function($elem) {return $elem['id'];}, $m);
            $in_form = array_map(function($elem) {if (isset($elem['id']) && $elem['id']) return $elem['id'];}, $movement);

            $diff = array_values(array_diff($in_mysql, $in_form));
            
            $for_delete = Movement::whereIn('id', $diff)->get()->toArray();

            foreach ($for_delete as $key => $value) {
                $part = Parts::find($value['parts_id']);
                $count = $part->count - $value['count'];
                if ($count < 0) $count = 0;
                $part->update(['count' => $count]);
            }
        }

        $dublicate = [];
        foreach ($movement as $key => $value) {
            
            if (!isset($value['part']['id']) || !$value['part']['id']) continue;
            if (in_array($value['part']['id'], $dublicate)) continue;

            array_push($dublicate, $value['part']['id']);

            /* Добавление новой запчасти в подотчет */
            if (!isset($value['id']) || !$value['id']) {
                $part = Parts::find($value['part']['id']);
                $part->update(['count' => $part->count + $value['count'], 'price' => $value['price']]);
                continue;
            }

            /* Обновление запчасти */
            foreach ($m as $k => $v) {
                if ($v['id'] != $value['id']) continue;

                $part = Parts::find($value['part']['id']);
                if ($v['part']['id'] == $value['part']['id']) {
                    
                    /* Если та же запчасть */
                    $update = [];
                    if ($v['price'] != $value['price']) $update['price'] = $value['price'];
                    if ($v['count'] != $value['count']) {
                        $diff = $v['count'] - $value['count'];
                        $update['count'] = $part->count - $diff;
                        if ($update['count'] < 0) $update['count'] = 0;
                    }

                    if ($update) $part->update($update);

                } else {

                    /* Если другая запчасть */
                    $count = $part->count - $v['count'];
                    if ($count < 0) $count = 0;
                    Parts::where('id', '=', $v['part']['id'])->update(['count' => $count]);
                    $part->update(['count' => $part->count + $value['count'], 'price' => $value['price']]);
                }
                
                unset($m[$k]);
                break;
            }
        }

        /* Синхронизация */
        $subreport->items()->sync($parts);
    }

    public function deleteReport($request, $response, $args) {
        $id = $request->getParams()['id'];
        $subreport = Subreport::with('movement')->find($id);
        foreach ($subreport['movement'] as $key => $value) {
        	$part = Parts::find($value['part']['id']);
        	$part->count -= $value['count'];
        	$part->save();
        	Movement::destroy($value['id']);
        }
        $subreport->delete();
    }
}