<?php

class IsAdmin
{

	private $container;

    public function __construct($container) {
    	$this->container = $container;
    }

    public function __invoke($request, $response, $next)
    {
        $user = $this->container['sentinel']->check();

        if ( $user && $user->inRole('admin') ) {
            return $response = $next($request, $response);
        } else {
            return $response->withStatus(301)->withHeader('Location', '/message/no-access');

        }
    }
}