<?php

class TestSeed {

    function run()
    {
        for ($i = 1; $i <= 30; $i++) {
            $partner = new Partner;
            $partner->name = "Test Partner ".$i;
            $partner->phone_mobile = "+792321366".$i;
            $partner->company_id = 6;
            $partner->save();
        }

    }
}
