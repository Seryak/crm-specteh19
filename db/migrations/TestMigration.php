<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class TestMigration {
    function run()
    {
        Capsule::schema()->dropIfExists('tests');
        Capsule::schema()->create('tests', function($table) {
            $table->increments('id');
            $table->string('username');
            $table->timestamps();
        });
    }
}
