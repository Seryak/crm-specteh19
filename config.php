<?php
$db = [
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'crmx',
    'username'  => 'root',
    'password'  => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
];