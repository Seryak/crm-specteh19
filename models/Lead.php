<?php

class Lead extends Illuminate\Database\Eloquent\Model
{
    protected $guarded = ['client'];

    public static function create(array $attributes = [])
    {
        if (!$attributes['tags']) {
            $attributes['tags'] = '';
        } else {
            $attributes['tags'] = implode(',', $attributes['tags']);
        }
        return parent::create($attributes);
    }



}