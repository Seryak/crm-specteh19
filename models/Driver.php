<?php
use Illuminate\Database\Eloquent\SoftDeletes;

class Driver extends Illuminate\Database\Eloquent\Model
{
    use SoftDeletes;
    public $timestamps = false;
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
    protected $guarded = [];

    public function setPhoneMobileAttribute($value)
    {
        $value = preg_replace('~[^0-9]+~','',$value);
        $this->attributes['phone_mobile'] = $value;
    }

    public function setPhoneMobile2Attribute($value)
    {
        $this->attributes['phone_mobile2'] = preg_replace('~[^0-9]+~','',$value);
    }

    public function setPhoneDopAttribute($value)
    {
        $this->attributes['phone_dop'] = preg_replace('~[^0-9]+~','',$value);
    }

    public function setPhoneCorpAttribute($value)
    {
        $this->attributes['phone_corp'] = preg_replace('~[^0-9]+~','',$value);
    }
}