<?php

class SettingsTransport extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'settings-transport';
}
