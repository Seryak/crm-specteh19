<?php
use Illuminate\Database\Eloquent\SoftDeletes;

class Partner extends Illuminate\Database\Eloquent\Model
{
    use SoftDeletes;
    public $timestamps = false;
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
    protected $guarded = [];

    public function transports()
    {
        return $this->hasMany('Transport', 'partner_id', 'id');
    }

    public function setPhoneMobileAttribute($value)
    {
        $this->attributes['phone_mobile'] = preg_replace('~[^0-9]+~','',$value);
    }

    public function setPhoneMobile2Attribute($value)
    {
        $this->attributes['phone_mobile2'] = preg_replace('~[^0-9]+~','',$value);
    }

    public function setPhoneDopAttribute($value)
    {
        $this->attributes['phone_dop'] = preg_replace('~[^0-9]+~','',$value);
    }
}