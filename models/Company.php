<?php

class Company extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function client()
    {
        return $this->hasMany('Client', 'company_id', 'id'); 
    }

    public function responsibles()
    {
        return $this->hasMany('Client', 'company_id', 'id')->where('responsible','=','1'); 
    }

    public function files()
    {
        return $this->belongsToMany('File');
    }

    public function setPhoneMobileAttribute($value)
    {
        $this->attributes['phone_mobile'] = preg_replace('~[^0-9]+~','',$value);
    }

    public function setPhoneMobile2Attribute($value)
    {
        $this->attributes['phone_mobile2'] = preg_replace('~[^0-9]+~','',$value);
    }

    public function setPhoneDopAttribute($value)
    {
        $this->attributes['phone_dop'] = preg_replace('~[^0-9]+~','',$value);
    }
}