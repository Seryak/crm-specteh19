<?php

class Client extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function company()
    {
        return $this->hasOne('Company', 'id', 'company_id');
    }

    public function setPhoneMobileAttribute($value)
    {
        $this->attributes['phone_mobile'] = preg_replace('~[^0-9]+~','',$value);
    }

    public function setPhoneMobile2Attribute($value)
    {
        $this->attributes['phone_mobile2'] = preg_replace('~[^0-9]+~','',$value);
    }

    public function setPhoneDopAttribute($value)
    {
        $this->attributes['phone_dop'] = preg_replace('~[^0-9]+~','',$value);
    }
}