<?php

use Illuminate\Database\Capsule\Manager as DB;

class Movement extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'movement';
    public $timestamps = true;
    protected $guarded = [];

    public function part() {
    	return $this->belongsTo('Parts', 'parts_id', 'id');
    }

    public function transport() {
    	return $this->belongsTo('TransportRepair', 'transport_id', 'id');
    }

    public function entity()
    {
        return $this->morphTo('entity');
    }

    public static function createPart($part, $movement) {
    	if ($part['title']) {
            DB::transaction(function() use ($part, $movement) {
        		$_part = Parts::create($part);
    	        $_movement = [
    	        'entity_type' => 'Subreport',
    	        'parts_id' => $_part->id,
    	        'count' => $part['count'],
    	        'price' => $part['price']
    	        ];
    	        $movement = array_merge($movement, $_movement);
        		Movement::create($movement);
        	});
        }
    }

    public static function addPart($part, $movement) {
    	DB::transaction(function() use ($part, $movement) {
	        $_part = Parts::find($part['id']);
	        $_part->count += $part['count'];
            $_part->price = $part['price'];
	        $_part->save();

	        $_movement = [
	        'entity_type' => 'Subreport',
	        'parts_id' => $_part->id,
	        'count' => $part['count'],
	        'price' => $part['price']
	        ];
	        $movement = array_merge($movement, $_movement);
    		Movement::create($movement);
    	});
    }

    public static function updatePart($part, $movement) {
    	DB::transaction(function() use ($part, $movement) {
    		$_part = Parts::find($part['id']);
            $_part->price = $part['price'];
    		$diff = $part['prev_count'] - $part['count'];
    		$_part->count -= $diff;
            if ($_part->count < 0) $_part->count = 0;
    		$_part->save();

    		$_movement = [
    		'entity_type' => 'Subreport',
    		'parts_id' => $_part->id,
    		'count' => $part['count'] > 0 ? $part['count'] : 0,
    		'price' => $part['price']
    		];
    		$movement = array_merge($movement, $_movement);

    		$row_movement = Movement::where('type', '=', 1)->where('parts_id', '=', $_part->id)->first();
    		$row_movement->update($movement);
    	});
    }

    public static function removePart($entity_id, $different) {
    	$for_delete = Movement::whereIn('id', $different)->get()->toArray();

        foreach ($for_delete as $item) {
    		DB::transaction(function() use ($entity_id, $item) {
	    			$_part = Parts::find($item['parts_id']);
	    			$_part->count = $_part->count - $item['count'];
                    if ($_part->count < 0) $_part->count = 0;
	    			$_part->save();
	    			Movement::destroy($item['id']);    				
    		});
    	}
    }

}