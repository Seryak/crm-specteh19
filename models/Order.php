<?php

class Order extends Illuminate\Database\Eloquent\Model
{
    protected $guarded = [];

    public function kind_transport()
    {
        return $this->hasOne('SettingsTransport', 'id', 'kind_transport');
    }

    public function setClientPhoneAttribute($value)
    {
        $this->attributes['client_phone'] = preg_replace('~[^0-9]+~','',$value);
    }

    public function client()
    {
        return $this->hasOne('Client', 'id', 'client_id');
    }

    public function company()
    {
        return $this->hasOne('Company', 'id', 'company_id');
    }

    public function files()
    {
        return $this->belongsToMany('File');
    }

    public function agent()
    {
        return $this->hasOne('Agent', 'id', 'agent_id');
    }
}
