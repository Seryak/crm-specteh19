<?php

class Subreport extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'subreports';
    public $timestamps = false;
    protected $guarded = [];

    public function items()
    {
        return $this->morphToMany('Parts', 'entity', 'movement', 'entity_id', 'parts_id');
    }

    public function movement()
    {
        return $this->morphMany('Movement', 'entity');
    }

    public function employer() {
    	return $this->belongsTo('Driver', 'drivers_id', 'id');
    }

}