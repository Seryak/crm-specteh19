<?php
use Illuminate\Database\Eloquent\SoftDeletes;

class Parts extends Illuminate\Database\Eloquent\Model
{
	use SoftDeletes;

	protected $table = 'parts';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $guarded = [];

    public function transport() {
    	return $this->belongsToMany('TransportRepair', 'parts_transportRepair', 'parts_id', 'transportRepair_id');
    }

}