<?php

class Card extends Model
{
    const table = 'cards';
    public $name;
    public $list_id;
    public $created_date;

    public function __construct($data)
    {
        parent::__construct($data);
        $this->created_date = date("Y-m-d");
    }

    public static function compare($old, $new)
    {
//        d($old);
//        d($new);

        foreach ($old as $key => $value){
            if ($old[$key] != $new[$key]) {
                Log::write('Изменилась cвойство ('.$key.' -> '.$new[$key].') у задачи "'.$old['name'].'"', 'card', $old['id']);
            }
        }

    }
}