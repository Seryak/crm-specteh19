<?php
//use Illuminate\Database\Eloquent\SoftDeletes;

class Transport extends Illuminate\Database\Eloquent\Model
{
//    use SoftDeletes;
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    protected $guarded = [];

    public function partner()
    {
        return $this->hasOne('Partner', 'id', 'partner_id');
    }

    public function kinds()
    {
        return $this->hasOne('SettingsTransport', 'id', 'kind_transport');
    }

    public function files()
    {
        return $this->belongsToMany('File');
    }

}