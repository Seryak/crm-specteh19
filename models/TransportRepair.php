<?php

class TransportRepair extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'transportRepair';
    public $timestamps = false;
    protected $guarded = [];

}