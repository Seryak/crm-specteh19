<?php

class Repair extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'repair';
    public $timestamps = false;
    protected $guarded = [];

    public function transport() {
    	return $this->belongsTo('TransportRepair', 'transportRepair_id', 'id');
    }

    public function drivers() {
    	return $this->belongsTo('Driver', 'drivers_id', 'id');
    }

    public function movement()
    {
        return $this->morphMany('Movement', 'entity');
    }

    public function parts()
    {
        return $this->morphToMany('Parts', 'entity', 'movement', 'entity_id', 'parts_id');
    }
}