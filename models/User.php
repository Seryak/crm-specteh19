<?php
use Cartalyst\Sentinel\Users\EloquentUser;
class User extends EloquentUser
{

    public $timestamps = false;
    protected $guarded = [];

    public function role() {
    	return $this->belongsToMany('Role', 'role_users', 'user_id', 'role_id');
    }    

//    public function login($login, $pass) {
//        $record = ORM::for_table('Users')->where('Username', $login)->find_one();
//        if ($record == FALSE) { return 'Такого логина нет';}
//        elseif ( $pass != $record->Password ) { return 'Неправильный пароль'; }
//        else {
//            if ( $pass == $record->Password )  {
//                $_SESSION['user']['access'] = 1;
//                $_SESSION['user']['name'] = $record->Username;
//                $_SESSION['user']['role'] = $record->role;
//                return 'Успешно!';
//            }
//        }
//
//
//    }

}
