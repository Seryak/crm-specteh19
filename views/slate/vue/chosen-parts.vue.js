Vue.component('chosen-parts', {
    delimiters: ['${', '}'],
    props: {
        parts: Array, 
        selected: [String, Number],
        disabled_parts: {
            type: [Array, Object],
            default: function() {
                return [];
            }
        }
    },

    data: function() {
        return {
            selected_parts: this.selected
        };
    },

    template:  '<select v-model="selected_parts" class="form-elem form-login"'
                + 'data-placeholder="Выберите запчасть" required>'
                + '<option value=""></option>'
                + '<option v-for="part in parts"'
                +     ':disabled="isDisabled(part.id)" :value="part.id"'
                +     ':style="{\'font-family\': \'Tahoma\', \'font-weight\': isDisabled(part.id) ? \'normal\' : \'bold\'}" >${part.title}</option>'
            + '</select>',

    methods: {
        isDisabled: function(id) {
            return this.selected != id && this.disabled_parts.indexOf(id) > -1;
        }        
    },

    updated: function() {
        $(this.$el).trigger("chosen:updated");
    },

    mounted: function() {
        var self = this;
        $(function() {
            $(self.$el).chosen({width: "100%", disable_search_threshold: 0}).change(function(e) {
                self.selected_parts = e.target.value;
                self.$emit('update:selected', self.selected_parts);
            });
        });
    }
});