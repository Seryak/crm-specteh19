$( document ).ready(function() {
    $('.add-project').click(function() {
        $('.popup-project').show('fast');
        $('.popup-project .add').attr('data-type', 'POST');
    });

    $('.project-list').on('click', '.card_footer a', function() {
        var id = $(this).closest('.project_card').attr('id');
        var el = $(this).closest('.card_footer');
        var name = el.siblings('.name').text();
        var description = el.siblings('.description').text();   
        $('.popup-project .add').attr('data-type', 'PUT');
        $('.popup-project .add').attr('data-project-id', id);
        $('.popup-project').show('fast');
        $('.popup-project .delete').css('display', 'inline-block');
        $('.popup-project .delete').attr('id', id);
        $('#name').val(name);
        $('#description').val(description);
    });

    $('.btn.cancel').click(function() {
        $('.popup-project').hide('fast');
    });

    $('.btn.delete').click(function() {
        var id = $(this).attr('id');
        $.ajax({
            url: '/api/task/'+id,
            type: 'DELETE',
            success: function(){
                $('.task#'+id).remove();
                $('.popup-task').hide('fast');
            }
        });

    });

    $('table.leads .show-desc').click(function() {
        var id = $(this).attr('data-id');
        var inst = $('.description-'+id).remodal();
        inst.open();
    });

    $('.btn.add').click(function() {
        var name = $('#name').val();
        var description = $('#description').val();
        var type = $(this).attr('data-type');
        var id = $(this).attr('data-project-id');
        $.ajax({
            url: '/api/project',
            type: type,
            data: "name="+name+"&description="+description+"&id="+id,
            success: function(data){
                if (type == 'PUT') {
                    $('.project_card#'+id).replaceWith(data);            
                } else {
                    $('.project-list').append(data);
                }
            }
        });

    });

    $(".chosen-select").chosen({width: "100%", disable_search_threshold: 0});

    if ($('select#agent').val() != '') {
        $('#paid-to-agent').fadeIn(150);
    }



    $('select#agent').on('change', function() {
        if ($('select#agent').val() != '') {
            $('#paid-to-agent').fadeIn(150);
        } else {
            $('#paid-to-agent').fadeOut(150);
        }
    });

    $('#agentClean').on('click', function(){
        $('#paid-to-agent').fadeOut(150);
    });
});