<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$c = $app->getContainer();
//Ремонт
$app->get('/admin/repairs', 'AdminController:showRepairs')->add(new Access($c, ['repairs_view']));
$app->get('/admin/repair/add[/{id}]', 'AdminController:showRepairAdd')->add(new Access($c, ['repairs_view']));

//Подотчеты
$app->get('/admin/subreports', 'AdminController:showSubreports')->add(new Access($c, ['subreports_view']));
$app->get('/admin/subreport[/{id}]', 'AdminController:showSubreportAdd')->add(new Access($c, ['subreports_view']));

$app->get('/admin/movement', 'AdminController:showMovement')->add(new Access($c, ['movement_view']));
//$app->post('/admin/movement', 'AdminController:showMovementFilter')->add(new Access($c, ['movement_view']));



$app->group('/admin', function() use($app) {

	$c = $app->getContainer();
	//Переименование конфига db
	$app->get('/stop', 'AdminController:offConfigDB')->add(new Access($c, ['partners_create']));
	$app->get('/start', 'AdminController:onConfigDB');

	// Лиды
	$app->get('/lead', 'AdminController:showLeadAdd');
	$app->get('/leads', 'AdminController:showLeadList');
	$app->get('/lead/{id}', 'AdminController:showLeadEdit');

	// Компании
	$app->get('/company/add', 'AdminController:showCompanyAdd')->add(new Access($c, ['company_view', 'company_create']));
	$app->get('/company', 'AdminController:showCompanyList')->add(new Access($c, ['company_view']));
	$app->get('/company/{id}', 'AdminController:showCompanyEdit')->add(new Access($c, ['company_view']));
	$app->get('/company/{id}/requisites', 'AdminController:showCompanyRequisites')->add(new Access($c, ['company_view']));
	$app->get('/company/{id}/workers', 'AdminController:showCompanyWorkers')->add(new Access($c, ['company_view']));

	//Сделки
	$app->get('/deals', 'AdminController:showDealList');
	$app->get('/deal', 'AdminController:showDealAdd');
	$app->get('/deal/{id}', 'AdminController:showDealEdit');

	//Клиенты
	$app->get('/client', 'AdminController:showClientAdd')->add(new Access($c, ['clients_view', 'clients_create']));
	$app->get('/clients', 'AdminController:showClientList')->add(new Access($c, ['clients_view']));
	$app->get('/client/{id}', 'AdminController:showClientEdit')->add(new Access($c, ['clients_view']));
	$app->get('/client/{id}/orders', 'AdminController:showClientOrders')->add(new Access($c, ['clients_view']));

	//Водители
	$app->get('/driver', 'AdminController:showDriverAdd')->add(new Access($c, ['drivers_view', 'drivers_create']));
	$app->get('/drivers', 'AdminController:showDriverList')->add(new Access($c, ['drivers_view']));
	$app->get('/drivers/trash', 'AdminController:showDriverListTrash')->add(new Access($c, ['drivers_view']));
	$app->get('/driver/{id}', 'AdminController:showDriverEdit')->add(new Access($c, ['drivers_view']));

	//Партнеры
	$app->get('/partner', 'AdminController:showPartnerAdd')->add(new Access($c, ['partners_view', 'partners_create']));
	$app->get('/partners', 'AdminController:showPartnerList')->add(new Access($c, ['partners_view']));
	$app->get('/partners/trash', 'AdminController:showPartnerListTrash')->add(new Access($c, ['partners_view']));
	$app->get('/partner/{id}', 'AdminController:showPartnerEdit')->add(new Access($c, ['partners_view']));

	//Агенты
    $app->get('/agent', 'AdminController:showAgentAdd')->add(new Access($c, ['partners_view', 'partners_create']));
    $app->get('/agents', 'AdminController:showAgentList')->add(new Access($c, ['partners_view']));
    $app->get('/agent/{id}', 'AdminController:showAgentEdit')->add(new Access($c, ['partners_view']));

	//Техника (транспорт)
	$app->get('/partner/{id}/transport', 'AdminController:showTransportAdd'); /* ?????  */
	$app->get('/transports', 'AdminController:showTransportList')->add(new Access($c, ['transports_view']));
	$app->get('/transport/{id}', 'AdminController:showTransportEdit')->add(new Access($c, ['transports_view']));

	//Запчасти
	$app->get('/parts', 'AdminController:showParts')->add(new Access($c, ['parts_view']));
	$app->get('/parts/excel', 'PartsController:exportCSV')->add(new Access($c, ['parts_view']));
	$app->get('/part[/{id}]', 'AdminController:showPartsAdd')->add(new Access($c, ['parts_view']));	
	$app->get('/parts/trashed', 'AdminController:showTrashedParts')->add(new Access($c, ['parts_view', 'parts_view']));
	$app->get('/part/trashed/{id}', 'AdminController:showPartTrashed')->add(new Access($c, ['parts_view', 'parts_delete']));
	
	//Заявки
	$app->get('/order', 'AdminController:showOrderAdd')->add(new Access($c, ['order_view', 'order_create']));
	$app->get('/orders', 'AdminController:showOrderList')->add(new Access($c, ['order_view']));
	$app->get('/orders/client/{id}', 'AdminController:showOrdersByClient')->add(new Access($c, ['order_view']));
	$app->get('/orders/company/{id}', 'AdminController:showOrdersByCompany')->add(new Access($c, ['order_view']));
	$app->get('/orders/partner/{id}', 'AdminController:showOrdersByPartner')->add(new Access($c, ['order_view']));
	$app->get('/orders/agent/{id}', 'AdminController:showOrdersByAgent')->add(new Access($c, ['order_view']));
	$app->get('/orders/driver/{id}', 'AdminController:showOrdersByDriver')->add(new Access($c, ['order_view']));
	$app->get('/order/{id}/copy', 'AdminController:showOrderCopy')->add(new Access($c, ['order_view', 'order_update']));
	$app->get('/order/{id}', 'AdminController:showOrderEdit')->add(new Access($c, ['order_view']));
});

$app->group('/admin', function() use($app) {

	$c = $app->getContainer();
	//Пользователи
	$app->get('/users', 'AdminController:showUserList')->add(new Access($c, ['user_view']));
	$app->get('/users/roles', 'AdminController:showUserRoles')->add(new Access($c, ['user_view', 'user_create']));
	$app->get('/user/{id}', 'AdminController:showUserEdit')->add(new Access($c, ['user_view', 'user_update']));
	$app->get('/user', 'AdminController:showUserAdd')->add(new Access($c, ['user_view', 'user_create']));

	$app->get('/settings', 'AdminController:showSettings')->add(new Access($c, ['settings_view']));
	$app->get('/settings/transport', 'AdminController:showSettingsTransport')->add(new Access($c, ['settings_view']));
	$app->get('/settings/transport/{id}', 'AdminController:showEditSettingsTransport')->add(new Access($c, ['settings_view', 'settings_update']));
	$app->get('/settings/transport_repair', 'AdminController:showSettingsTransportRepair')->add(new Access($c, ['settings_view']));
	$app->get('/settings/transport_repair/{id}', 'AdminController:showEditSettingsTransportRepair')->add(new Access($c, ['settings_view', 'settings_update']));
});