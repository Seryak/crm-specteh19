<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$c = $app->getContainer();
$app->post('/api/login', 'UserController:doLogin');
$app->get('/api/logout', 'UserController:doLogout');

//Ремонт
$app->post('/api/parts_for_tech', 'PartsController:getPartsForTech');
$app->post('/api/repair/create', 'RepairController:createRepair')->add(new Access($c, ['repairs_create']));
$app->post('/api/repair/update', 'RepairController:updateRepair')->add(new Access($c, ['repairs_update']));
$app->post('/api/repair/delete', 'RepairController:deleteRepair')->add(new Access($c, ['repairs_delete']));

/* Экспорт клиентов в CSV */
$app->get('/api/clients/export_csv', 'ClientController:exportCSV')->add(new Access($c, ['clients_view']));
$app->get('/api/transport/export_csv', 'TransportController:exportCSV')->add(new Access($c, ['transports_view']));

//Подотчеты
$app->post('/api/subreport/create', 'SubreportController:createReport')->add(new Access($c, ['subreports_create']));

$app->get('/message/{message}', 'AdminController:showMessage');

$app->get('/api/autocomplete/parts', 'PartsController:autocompleteParts');

$app->post('/api/list/sync', function (Request $request, Response $response) {
    $data = $request->getParams();
    $list = json_decode($data['json']);
    foreach ($list->cards as $card) {
        $record =  Card::load($card->id);
        $record->position = $card->pos;
        $record->save();
    }
});



$app->post('/api/file', function (Request $request, Response $response) {
    $data = $request->getParams();
    $file = new File($_FILES['file'], $data);
    ddd($file);
});

$app->group('', function() use ($app) {
    $c = $app->getContainer();

    $app->post('/api/subreport/delete', 'SubreportController:deleteReport')->add(new Access($c, ['subreports_delete']));

    // Лиды
    $app->get('/api/delete/lead/{id}', 'LeadController:deleteLead');
    $app->post('/api/lead', 'LeadController:createLead');
    $app->post('/api/lead/{id}', 'LeadController:editLead');

    //Сделки
    $app->post('/api/deal', 'DealController:createDeal');
    $app->post('/api/deal/{id}', 'DealController:editDeal');
    $app->get('/api/delete/deal/{id}', 'DealController:deleteDeal');

    //Компании
    $app->post('/api/company', 'CompanyController:createCompany')->add(new Access($c, ['company_create']));
    $app->post('/api/company/{id}', 'CompanyController:editCompany')->add(new Access($c, ['company_update']));
    //$app->post('/api/company/{id}/requisites', 'CompanyController:editCompanyRequisites');

    //Клиенты
    $app->post('/api/client', 'ClientController:createClient')->add(new Access($c, ['clients_create']));
    $app->post('/api/client/{id}', 'ClientController:editClient')->add(new Access($c, ['clients_update']));
    $app->get('/api/delete/client/{id}', 'ClientController:deleteClient')->add(new Access($c, ['clients_delete']));

    //Водители
    $app->post('/api/driver', 'DriverController:createDriver')->add(new Access($c, ['drivers_create']));
    $app->post('/api/driver/{id}', 'DriverController:editDriver')->add(new Access($c, ['drivers_update']));
    $app->get('/api/delete/driver/{id}', 'DriverController:deleteDriver')->add(new Access($c, ['drivers_delete']));

    //Партнеры
    $app->post('/api/partner', 'PartnerController:createPartner')->add(new Access($c, ['partners_create']));
    $app->post('/api/partner/{id}', 'PartnerController:editPartner')->add(new Access($c, ['partners_update']));
    $app->get('/api/delete/partner/{id}', 'PartnerController:deletePartner')->add(new Access($c, ['partners_delete']));

    //Агенты
    $app->post('/api/agent', 'AgentController:createAgent')->add(new Access($c, ['partners_create']));
    $app->post('/api/agent/{id}', 'AgentController:editAgent')->add(new Access($c, ['partners_update']));
    $app->get('/api/delete/agent/{id}', 'AgentController:deleteAgent')->add(new Access($c, ['partners_delete']));

    //Транспорт
    $app->post('/api/transport', 'TransportController:createTransport')->add(new Access($c, ['transports_create']));
    $app->post('/api/transport/{id}', 'TransportController:editTransport')->add(new Access($c, ['transports_update']));
    $app->get('/api/delete/transport/{id}', 'TransportController:deleteTransport')->add(new Access($c, ['transports_delete']));

    //Запчасти
    $app->post('/api/parts/add', 'PartsController:createParts')->add(new Access($c, ['parts_create']));
    $app->post('/api/parts/update/{id}', 'PartsController:editParts')->add(new Access($c, ['parts_update']));
    $app->post('/api/parts/restore/{id}', 'PartsController:restorePart')->add(new Access($c, ['parts_delete']));
    $app->post('/api/parts/delete/{id}', 'PartsController:deleteParts')->add(new Access($c, ['parts_delete']));
    $app->post('/api/parts/delete_forever/{id}', 'PartsController:deletePartsForever')->add(new Access($c, ['parts_delete']));

    $app->post('/api/order', 'OrderController:createOrder')->add(new Access($c, ['order_create']));
    $app->post('/api/order/{id}', 'OrderController:updateOrder')->add(new Access($c, ['order_update']));
    $app->get('/api/delete/order/{id}', 'OrderController:deleteOrder')->add(new Access($c, ['order_delete']));
    //$app->post('/api/transport/{id}', 'TransportController:editTransport');
    //$app->get('/api/delete/transport/{id}', 'TransportController:deleteTransport')->add( new IsAdmin() );

    $app->post('/api/user', 'UserController:createUser')->add(new Access($c, ['user_create']));
    $app->post('/api/users/roles/get_role', 'UserController:getRole')->add(new Access($c, ['user_view']));
    $app->post('/api/users/roles/create', 'UserController:createRole')->add(new Access($c, ['user_create']));
    $app->post('/api/user/{id}', 'UserController:editUser')->add(new Access($c, ['user_update']));
    $app->post('/api/delete/user/{id}', 'UserController:deleteUser')->add(new Access($c, ['user_delete']));

    $app->post('/api/settings-transport', 'TransportController:createSettingsTransport')->add(new Access($c, ['settings_create']));
    $app->post('/api/settings-transport/{id}', 'TransportController:updateSettingsTransport')->add(new Access($c, ['settings_update']));
    $app->post('/api/settings-transport_repair', 'TransportController:createSettingsTransportRepair')->add(new Access($c, ['settings_create']));
    $app->post('/api/settings-transport_repair/{id}', 'TransportController:updateSettingsTransportRepair')->add(new Access($c, ['settings_update']));

    $app->get('/api/delete/file/{id}', 'AdminController:deleteFile');


    $app->get('/api/check/uniq-phone/{value}', 'AdminController:checkUniqPhone');
    $app->get('/api/check/mobil-phone/{value}', 'AdminController:checkMobilePhone');
    $app->get('/api/check/{table}/{field}/{value}', 'AdminController:checkUniq');
    $app->get('/api/autocomplete/client', 'AdminController:autocompleteClient');
    $app->get('/api/autocomplete/company', 'CompanyController:autocompleteCompany');
});